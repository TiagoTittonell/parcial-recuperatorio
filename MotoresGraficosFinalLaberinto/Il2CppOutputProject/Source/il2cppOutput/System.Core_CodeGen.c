﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 (void);
// 0x00000002 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 (void);
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 (void);
// 0x00000004 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 (void);
// 0x00000005 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000007 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000008 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000009 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000B System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000C System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Distinct(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::DistinctIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000000F TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000010 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000011 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000012 TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000013 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000014 TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000015 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000016 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000017 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000018 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000019 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x0000001A System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000001B System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000001C TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000001D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x0000001E System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x0000001F System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000020 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000021 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000022 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000023 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000024 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000025 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000026 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000027 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000028 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000029 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000002A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000002B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002C System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x0000002D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x0000002E System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x0000002F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000030 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000031 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000032 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000033 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000034 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000035 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000036 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000037 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x00000038 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x00000039 System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000003A System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000003B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000003C System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000003D System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x0000003E System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x0000003F System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000040 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000041 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000042 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000043 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x00000044 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000045 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000046 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000047 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000048 System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x00000049 TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000004A System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x0000004B System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x0000004C System.Boolean System.Linq.Enumerable_<SelectManyIterator>d__17`2::MoveNext()
// 0x0000004D System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x0000004E System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x0000004F TResult System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000050 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x00000051 System.Object System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x00000052 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000053 System.Collections.IEnumerator System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000054 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::.ctor(System.Int32)
// 0x00000055 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.IDisposable.Dispose()
// 0x00000056 System.Boolean System.Linq.Enumerable_<DistinctIterator>d__68`1::MoveNext()
// 0x00000057 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::<>m__Finally1()
// 0x00000058 TSource System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000059 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.Reset()
// 0x0000005A System.Object System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.get_Current()
// 0x0000005B System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000005C System.Collections.IEnumerator System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000005D System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000005E System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x0000005F System.Boolean System.Linq.Set`1::Add(TElement)
// 0x00000060 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x00000061 System.Void System.Linq.Set`1::Resize()
// 0x00000062 System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x00000063 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x00000064 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000065 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000066 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000067 System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x00000068 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x00000069 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x0000006A System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x0000006B TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x0000006C System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x0000006D System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x0000006E System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000006F System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000070 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x00000071 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x00000072 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x00000073 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x00000074 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x00000075 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x00000076 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x00000077 System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x00000078 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000079 TElement[] System.Linq.Buffer`1::ToArray()
// 0x0000007A System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x0000007B System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000007C System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000007D System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x0000007E System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x0000007F System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000080 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000081 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000082 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000083 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000084 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000085 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000086 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000087 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000088 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000089 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x0000008A System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x0000008B System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x0000008C System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x0000008D System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x0000008E System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x0000008F System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000090 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000091 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000092 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x00000093 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x00000094 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x00000095 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000096 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[150] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[150] = 
{
	0,
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[49] = 
{
	{ 0x02000004, { 73, 4 } },
	{ 0x02000005, { 77, 9 } },
	{ 0x02000006, { 88, 7 } },
	{ 0x02000007, { 97, 10 } },
	{ 0x02000008, { 109, 11 } },
	{ 0x02000009, { 123, 9 } },
	{ 0x0200000A, { 135, 12 } },
	{ 0x0200000B, { 150, 1 } },
	{ 0x0200000C, { 151, 2 } },
	{ 0x0200000D, { 153, 12 } },
	{ 0x0200000E, { 165, 11 } },
	{ 0x02000010, { 176, 8 } },
	{ 0x02000012, { 184, 3 } },
	{ 0x02000013, { 189, 5 } },
	{ 0x02000014, { 194, 7 } },
	{ 0x02000015, { 201, 3 } },
	{ 0x02000016, { 204, 7 } },
	{ 0x02000017, { 211, 4 } },
	{ 0x02000018, { 215, 21 } },
	{ 0x0200001A, { 236, 2 } },
	{ 0x06000005, { 0, 10 } },
	{ 0x06000006, { 10, 10 } },
	{ 0x06000007, { 20, 5 } },
	{ 0x06000008, { 25, 5 } },
	{ 0x06000009, { 30, 1 } },
	{ 0x0600000A, { 31, 2 } },
	{ 0x0600000B, { 33, 2 } },
	{ 0x0600000C, { 35, 1 } },
	{ 0x0600000D, { 36, 1 } },
	{ 0x0600000E, { 37, 2 } },
	{ 0x0600000F, { 39, 3 } },
	{ 0x06000010, { 42, 2 } },
	{ 0x06000011, { 44, 3 } },
	{ 0x06000012, { 47, 4 } },
	{ 0x06000013, { 51, 3 } },
	{ 0x06000014, { 54, 3 } },
	{ 0x06000015, { 57, 1 } },
	{ 0x06000016, { 58, 3 } },
	{ 0x06000017, { 61, 2 } },
	{ 0x06000018, { 63, 3 } },
	{ 0x06000019, { 66, 2 } },
	{ 0x0600001A, { 68, 5 } },
	{ 0x0600002A, { 86, 2 } },
	{ 0x0600002F, { 95, 2 } },
	{ 0x06000034, { 107, 2 } },
	{ 0x0600003A, { 120, 3 } },
	{ 0x0600003F, { 132, 3 } },
	{ 0x06000044, { 147, 3 } },
	{ 0x06000066, { 187, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[238] = 
{
	{ (Il2CppRGCTXDataType)2, 24113 },
	{ (Il2CppRGCTXDataType)3, 18948 },
	{ (Il2CppRGCTXDataType)2, 24114 },
	{ (Il2CppRGCTXDataType)2, 24115 },
	{ (Il2CppRGCTXDataType)3, 18949 },
	{ (Il2CppRGCTXDataType)2, 24116 },
	{ (Il2CppRGCTXDataType)2, 24117 },
	{ (Il2CppRGCTXDataType)3, 18950 },
	{ (Il2CppRGCTXDataType)2, 24118 },
	{ (Il2CppRGCTXDataType)3, 18951 },
	{ (Il2CppRGCTXDataType)2, 24119 },
	{ (Il2CppRGCTXDataType)3, 18952 },
	{ (Il2CppRGCTXDataType)2, 24120 },
	{ (Il2CppRGCTXDataType)2, 24121 },
	{ (Il2CppRGCTXDataType)3, 18953 },
	{ (Il2CppRGCTXDataType)2, 24122 },
	{ (Il2CppRGCTXDataType)2, 24123 },
	{ (Il2CppRGCTXDataType)3, 18954 },
	{ (Il2CppRGCTXDataType)2, 24124 },
	{ (Il2CppRGCTXDataType)3, 18955 },
	{ (Il2CppRGCTXDataType)2, 24125 },
	{ (Il2CppRGCTXDataType)3, 18956 },
	{ (Il2CppRGCTXDataType)3, 18957 },
	{ (Il2CppRGCTXDataType)2, 17388 },
	{ (Il2CppRGCTXDataType)3, 18958 },
	{ (Il2CppRGCTXDataType)2, 24126 },
	{ (Il2CppRGCTXDataType)3, 18959 },
	{ (Il2CppRGCTXDataType)3, 18960 },
	{ (Il2CppRGCTXDataType)2, 17395 },
	{ (Il2CppRGCTXDataType)3, 18961 },
	{ (Il2CppRGCTXDataType)3, 18962 },
	{ (Il2CppRGCTXDataType)2, 24127 },
	{ (Il2CppRGCTXDataType)3, 18963 },
	{ (Il2CppRGCTXDataType)2, 24128 },
	{ (Il2CppRGCTXDataType)3, 18964 },
	{ (Il2CppRGCTXDataType)3, 18965 },
	{ (Il2CppRGCTXDataType)3, 18966 },
	{ (Il2CppRGCTXDataType)2, 24129 },
	{ (Il2CppRGCTXDataType)3, 18967 },
	{ (Il2CppRGCTXDataType)2, 24130 },
	{ (Il2CppRGCTXDataType)3, 18968 },
	{ (Il2CppRGCTXDataType)3, 18969 },
	{ (Il2CppRGCTXDataType)2, 17425 },
	{ (Il2CppRGCTXDataType)3, 18970 },
	{ (Il2CppRGCTXDataType)2, 17426 },
	{ (Il2CppRGCTXDataType)2, 24131 },
	{ (Il2CppRGCTXDataType)3, 18971 },
	{ (Il2CppRGCTXDataType)2, 24132 },
	{ (Il2CppRGCTXDataType)2, 24133 },
	{ (Il2CppRGCTXDataType)2, 17429 },
	{ (Il2CppRGCTXDataType)2, 24134 },
	{ (Il2CppRGCTXDataType)2, 17431 },
	{ (Il2CppRGCTXDataType)2, 24135 },
	{ (Il2CppRGCTXDataType)3, 18972 },
	{ (Il2CppRGCTXDataType)2, 24136 },
	{ (Il2CppRGCTXDataType)2, 17434 },
	{ (Il2CppRGCTXDataType)2, 24137 },
	{ (Il2CppRGCTXDataType)2, 17436 },
	{ (Il2CppRGCTXDataType)2, 17438 },
	{ (Il2CppRGCTXDataType)2, 24138 },
	{ (Il2CppRGCTXDataType)3, 18973 },
	{ (Il2CppRGCTXDataType)2, 24139 },
	{ (Il2CppRGCTXDataType)2, 17441 },
	{ (Il2CppRGCTXDataType)2, 17443 },
	{ (Il2CppRGCTXDataType)2, 24140 },
	{ (Il2CppRGCTXDataType)3, 18974 },
	{ (Il2CppRGCTXDataType)2, 24141 },
	{ (Il2CppRGCTXDataType)3, 18975 },
	{ (Il2CppRGCTXDataType)3, 18976 },
	{ (Il2CppRGCTXDataType)2, 24142 },
	{ (Il2CppRGCTXDataType)2, 17448 },
	{ (Il2CppRGCTXDataType)2, 24143 },
	{ (Il2CppRGCTXDataType)2, 17450 },
	{ (Il2CppRGCTXDataType)3, 18977 },
	{ (Il2CppRGCTXDataType)3, 18978 },
	{ (Il2CppRGCTXDataType)2, 17453 },
	{ (Il2CppRGCTXDataType)3, 18979 },
	{ (Il2CppRGCTXDataType)3, 18980 },
	{ (Il2CppRGCTXDataType)2, 17465 },
	{ (Il2CppRGCTXDataType)2, 24144 },
	{ (Il2CppRGCTXDataType)3, 18981 },
	{ (Il2CppRGCTXDataType)3, 18982 },
	{ (Il2CppRGCTXDataType)2, 17467 },
	{ (Il2CppRGCTXDataType)2, 23975 },
	{ (Il2CppRGCTXDataType)3, 18983 },
	{ (Il2CppRGCTXDataType)3, 18984 },
	{ (Il2CppRGCTXDataType)2, 24145 },
	{ (Il2CppRGCTXDataType)3, 18985 },
	{ (Il2CppRGCTXDataType)3, 18986 },
	{ (Il2CppRGCTXDataType)2, 17477 },
	{ (Il2CppRGCTXDataType)2, 24146 },
	{ (Il2CppRGCTXDataType)3, 18987 },
	{ (Il2CppRGCTXDataType)3, 18988 },
	{ (Il2CppRGCTXDataType)3, 17716 },
	{ (Il2CppRGCTXDataType)3, 18989 },
	{ (Il2CppRGCTXDataType)2, 24147 },
	{ (Il2CppRGCTXDataType)3, 18990 },
	{ (Il2CppRGCTXDataType)3, 18991 },
	{ (Il2CppRGCTXDataType)2, 17489 },
	{ (Il2CppRGCTXDataType)2, 24148 },
	{ (Il2CppRGCTXDataType)3, 18992 },
	{ (Il2CppRGCTXDataType)3, 18993 },
	{ (Il2CppRGCTXDataType)3, 18994 },
	{ (Il2CppRGCTXDataType)3, 18995 },
	{ (Il2CppRGCTXDataType)3, 18996 },
	{ (Il2CppRGCTXDataType)3, 17722 },
	{ (Il2CppRGCTXDataType)3, 18997 },
	{ (Il2CppRGCTXDataType)2, 24149 },
	{ (Il2CppRGCTXDataType)3, 18998 },
	{ (Il2CppRGCTXDataType)3, 18999 },
	{ (Il2CppRGCTXDataType)2, 17502 },
	{ (Il2CppRGCTXDataType)2, 24150 },
	{ (Il2CppRGCTXDataType)3, 19000 },
	{ (Il2CppRGCTXDataType)3, 19001 },
	{ (Il2CppRGCTXDataType)2, 17504 },
	{ (Il2CppRGCTXDataType)2, 24151 },
	{ (Il2CppRGCTXDataType)3, 19002 },
	{ (Il2CppRGCTXDataType)3, 19003 },
	{ (Il2CppRGCTXDataType)2, 24152 },
	{ (Il2CppRGCTXDataType)3, 19004 },
	{ (Il2CppRGCTXDataType)3, 19005 },
	{ (Il2CppRGCTXDataType)2, 24153 },
	{ (Il2CppRGCTXDataType)3, 19006 },
	{ (Il2CppRGCTXDataType)3, 19007 },
	{ (Il2CppRGCTXDataType)2, 17519 },
	{ (Il2CppRGCTXDataType)2, 24154 },
	{ (Il2CppRGCTXDataType)3, 19008 },
	{ (Il2CppRGCTXDataType)3, 19009 },
	{ (Il2CppRGCTXDataType)3, 19010 },
	{ (Il2CppRGCTXDataType)3, 17733 },
	{ (Il2CppRGCTXDataType)2, 24155 },
	{ (Il2CppRGCTXDataType)3, 19011 },
	{ (Il2CppRGCTXDataType)3, 19012 },
	{ (Il2CppRGCTXDataType)2, 24156 },
	{ (Il2CppRGCTXDataType)3, 19013 },
	{ (Il2CppRGCTXDataType)3, 19014 },
	{ (Il2CppRGCTXDataType)2, 17535 },
	{ (Il2CppRGCTXDataType)2, 24157 },
	{ (Il2CppRGCTXDataType)3, 19015 },
	{ (Il2CppRGCTXDataType)3, 19016 },
	{ (Il2CppRGCTXDataType)3, 19017 },
	{ (Il2CppRGCTXDataType)3, 19018 },
	{ (Il2CppRGCTXDataType)3, 19019 },
	{ (Il2CppRGCTXDataType)3, 19020 },
	{ (Il2CppRGCTXDataType)3, 17739 },
	{ (Il2CppRGCTXDataType)2, 24158 },
	{ (Il2CppRGCTXDataType)3, 19021 },
	{ (Il2CppRGCTXDataType)3, 19022 },
	{ (Il2CppRGCTXDataType)2, 24159 },
	{ (Il2CppRGCTXDataType)3, 19023 },
	{ (Il2CppRGCTXDataType)3, 19024 },
	{ (Il2CppRGCTXDataType)3, 19025 },
	{ (Il2CppRGCTXDataType)3, 19026 },
	{ (Il2CppRGCTXDataType)3, 19027 },
	{ (Il2CppRGCTXDataType)3, 19028 },
	{ (Il2CppRGCTXDataType)2, 24160 },
	{ (Il2CppRGCTXDataType)2, 24161 },
	{ (Il2CppRGCTXDataType)3, 19029 },
	{ (Il2CppRGCTXDataType)2, 17570 },
	{ (Il2CppRGCTXDataType)2, 17564 },
	{ (Il2CppRGCTXDataType)3, 19030 },
	{ (Il2CppRGCTXDataType)2, 17563 },
	{ (Il2CppRGCTXDataType)2, 24162 },
	{ (Il2CppRGCTXDataType)3, 19031 },
	{ (Il2CppRGCTXDataType)3, 19032 },
	{ (Il2CppRGCTXDataType)3, 19033 },
	{ (Il2CppRGCTXDataType)2, 24163 },
	{ (Il2CppRGCTXDataType)3, 19034 },
	{ (Il2CppRGCTXDataType)2, 17586 },
	{ (Il2CppRGCTXDataType)2, 17578 },
	{ (Il2CppRGCTXDataType)3, 19035 },
	{ (Il2CppRGCTXDataType)3, 19036 },
	{ (Il2CppRGCTXDataType)2, 17577 },
	{ (Il2CppRGCTXDataType)2, 24164 },
	{ (Il2CppRGCTXDataType)3, 19037 },
	{ (Il2CppRGCTXDataType)3, 19038 },
	{ (Il2CppRGCTXDataType)3, 19039 },
	{ (Il2CppRGCTXDataType)2, 24165 },
	{ (Il2CppRGCTXDataType)2, 24166 },
	{ (Il2CppRGCTXDataType)3, 19040 },
	{ (Il2CppRGCTXDataType)3, 19041 },
	{ (Il2CppRGCTXDataType)2, 17598 },
	{ (Il2CppRGCTXDataType)3, 19042 },
	{ (Il2CppRGCTXDataType)2, 17599 },
	{ (Il2CppRGCTXDataType)2, 24167 },
	{ (Il2CppRGCTXDataType)3, 19043 },
	{ (Il2CppRGCTXDataType)3, 19044 },
	{ (Il2CppRGCTXDataType)2, 24168 },
	{ (Il2CppRGCTXDataType)3, 19045 },
	{ (Il2CppRGCTXDataType)2, 24169 },
	{ (Il2CppRGCTXDataType)3, 19046 },
	{ (Il2CppRGCTXDataType)3, 19047 },
	{ (Il2CppRGCTXDataType)3, 19048 },
	{ (Il2CppRGCTXDataType)2, 17618 },
	{ (Il2CppRGCTXDataType)3, 19049 },
	{ (Il2CppRGCTXDataType)2, 17626 },
	{ (Il2CppRGCTXDataType)3, 19050 },
	{ (Il2CppRGCTXDataType)2, 24170 },
	{ (Il2CppRGCTXDataType)2, 24171 },
	{ (Il2CppRGCTXDataType)3, 19051 },
	{ (Il2CppRGCTXDataType)3, 19052 },
	{ (Il2CppRGCTXDataType)3, 19053 },
	{ (Il2CppRGCTXDataType)3, 19054 },
	{ (Il2CppRGCTXDataType)3, 19055 },
	{ (Il2CppRGCTXDataType)3, 19056 },
	{ (Il2CppRGCTXDataType)2, 17642 },
	{ (Il2CppRGCTXDataType)2, 24172 },
	{ (Il2CppRGCTXDataType)3, 19057 },
	{ (Il2CppRGCTXDataType)3, 19058 },
	{ (Il2CppRGCTXDataType)2, 17646 },
	{ (Il2CppRGCTXDataType)3, 19059 },
	{ (Il2CppRGCTXDataType)2, 24173 },
	{ (Il2CppRGCTXDataType)2, 17656 },
	{ (Il2CppRGCTXDataType)2, 17654 },
	{ (Il2CppRGCTXDataType)2, 24174 },
	{ (Il2CppRGCTXDataType)3, 19060 },
	{ (Il2CppRGCTXDataType)2, 24175 },
	{ (Il2CppRGCTXDataType)3, 19061 },
	{ (Il2CppRGCTXDataType)3, 19062 },
	{ (Il2CppRGCTXDataType)3, 19063 },
	{ (Il2CppRGCTXDataType)2, 17660 },
	{ (Il2CppRGCTXDataType)3, 19064 },
	{ (Il2CppRGCTXDataType)3, 19065 },
	{ (Il2CppRGCTXDataType)2, 17663 },
	{ (Il2CppRGCTXDataType)3, 19066 },
	{ (Il2CppRGCTXDataType)1, 24176 },
	{ (Il2CppRGCTXDataType)2, 17662 },
	{ (Il2CppRGCTXDataType)3, 19067 },
	{ (Il2CppRGCTXDataType)1, 17662 },
	{ (Il2CppRGCTXDataType)1, 17660 },
	{ (Il2CppRGCTXDataType)2, 24177 },
	{ (Il2CppRGCTXDataType)2, 17662 },
	{ (Il2CppRGCTXDataType)3, 19068 },
	{ (Il2CppRGCTXDataType)3, 19069 },
	{ (Il2CppRGCTXDataType)3, 19070 },
	{ (Il2CppRGCTXDataType)2, 17661 },
	{ (Il2CppRGCTXDataType)3, 19071 },
	{ (Il2CppRGCTXDataType)2, 17674 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	150,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	49,
	s_rgctxIndices,
	238,
	s_rgctxValues,
	NULL,
};
