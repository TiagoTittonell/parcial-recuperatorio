﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void GunView::OnEnable()
extern void GunView_OnEnable_mDF7A5EA7152DEE56CBE42A3865C92289F8A7F9C7 (void);
// 0x00000002 System.Void GunView::Update()
extern void GunView_Update_mB2DCAD57A5AF2ED1246E7FCD25E131970FE9F395 (void);
// 0x00000003 System.Void GunView::ScaleUp()
extern void GunView_ScaleUp_m4A0713AEDE03F15B8C205C75623790B8A04590B6 (void);
// 0x00000004 System.Void GunView::ScaleDown()
extern void GunView_ScaleDown_m4AED203F54F6727CF3EF94FD23F669BA8DBE4378 (void);
// 0x00000005 System.Void GunView::.ctor()
extern void GunView__ctor_m8DDE2842E8D62472387B1F2A2C84A2C3AB138AFC (void);
// 0x00000006 System.Void GunsMenu::Start()
extern void GunsMenu_Start_m096DF81DB4260C440D835B3BC8D9C99A7F8B9B81 (void);
// 0x00000007 System.Void GunsMenu::NextGun()
extern void GunsMenu_NextGun_m2066B1B817B4AD3A7ECD9EA4E5EF91069588867E (void);
// 0x00000008 System.Void GunsMenu::PreviousGun()
extern void GunsMenu_PreviousGun_m7F5F5965720D161E69B887DEC7A7C9A5BB5EA412 (void);
// 0x00000009 System.Void GunsMenu::Update()
extern void GunsMenu_Update_m1DCE6B02B9A1D113AC2EB02A6D53D3A9B84C85F5 (void);
// 0x0000000A System.Void GunsMenu::.ctor()
extern void GunsMenu__ctor_m1912EF0FC8BF58D7ABAE3F76FFD1D5CA856942F2 (void);
// 0x0000000B System.Void BarraDeVida::Update()
extern void BarraDeVida_Update_m05AECAD272D29710D4D5ABB2406F08C8ED434F64 (void);
// 0x0000000C System.Void BarraDeVida::.ctor()
extern void BarraDeVida__ctor_m73116A91A1BF25F308683DB3DD9B55EE78409E5C (void);
// 0x0000000D System.Void Configuracion::Start()
extern void Configuracion_Start_m9FD3D83B84D20DA6187983E8C21415DA55360E17 (void);
// 0x0000000E System.Void Configuracion::Update()
extern void Configuracion_Update_m6DE8A6EF7B628D3B5F30336D16EF41496FF8747A (void);
// 0x0000000F System.Void Configuracion::.ctor()
extern void Configuracion__ctor_m516E953C50A2B4F82EC5B5C98024DA1C2BFF920B (void);
// 0x00000010 System.Void ControlBot::Start()
extern void ControlBot_Start_m516CAA6BE2325425DA986908B25CA69E316A0DE1 (void);
// 0x00000011 System.Void ControlBot::Update()
extern void ControlBot_Update_mB215B939A4E82D6B449988C7EB784B9EB9BEE7E3 (void);
// 0x00000012 System.Void ControlBot::OnCollisionEnter(UnityEngine.Collision)
extern void ControlBot_OnCollisionEnter_m03319A868CB93DB58D37F4DED8E2A3F2356ABE35 (void);
// 0x00000013 System.Void ControlBot::recibirDaUF1o()
extern void ControlBot_recibirDaUF1o_m9821E5F8B00224DE63E2BD0BF51435BDB320E7FC (void);
// 0x00000014 System.Void ControlBot::desaparecer()
extern void ControlBot_desaparecer_m6D0B728AA01917F7473C2245FD960232A7E87EA3 (void);
// 0x00000015 System.Void ControlBot::.ctor()
extern void ControlBot__ctor_m9C42F6EEA929FBC5B5C92A018012B8323D16D6C6 (void);
// 0x00000016 System.Void ControlJefe::Start()
extern void ControlJefe_Start_m5FA8EE6AE08EDFCC368AA2C29461C846E65CDCD4 (void);
// 0x00000017 System.Void ControlJefe::Update()
extern void ControlJefe_Update_mE41551C23C0307AEA12FD4C984F75D63B30B1513 (void);
// 0x00000018 System.Void ControlJefe::OnCollisionEnter(UnityEngine.Collision)
extern void ControlJefe_OnCollisionEnter_mFB4333A602D061D49942A66D693E724200EA24E9 (void);
// 0x00000019 System.Void ControlJefe::recibirDaUF1o()
extern void ControlJefe_recibirDaUF1o_mC1510ACB15FBAF4186B3775426853456D6FF8987 (void);
// 0x0000001A System.Void ControlJefe::desaparecer()
extern void ControlJefe_desaparecer_m40C1B5B2225D8387FEECD82633CBBF6362A002F7 (void);
// 0x0000001B System.Void ControlJefe::.ctor()
extern void ControlJefe__ctor_mFBC35769F5CA1F9EA4353733D535484C0C5A3A4A (void);
// 0x0000001C System.Void ControlJugador::Start()
extern void ControlJugador_Start_m4E2BC1B8AEF94CFA13FA0A675ECC6CDD7C9E2546 (void);
// 0x0000001D System.Void ControlJugador::Update()
extern void ControlJugador_Update_m3C5ABBBBF954C1FB58E04A8E18F75DF313BE6FE8 (void);
// 0x0000001E System.Void ControlJugador::.ctor()
extern void ControlJugador__ctor_m8081D7912A48835FE1D245E0CA7E6DD5D2E868CB (void);
// 0x0000001F System.Void ControlJugador1::Start()
extern void ControlJugador1_Start_m4CBE46086B7ECDD4C2DF0A9AEB0C8216D94A06DD (void);
// 0x00000020 System.Void ControlJugador1::update()
extern void ControlJugador1_update_m18022A2807EA1E23609E204132F1CE213DB1458C (void);
// 0x00000021 System.Void ControlJugador1::.ctor()
extern void ControlJugador1__ctor_m0AD9757AFB101D015DA3B6AB419E9BB6D58A30DB (void);
// 0x00000022 System.Void ControlMenuprincipal::.ctor()
extern void ControlMenuprincipal__ctor_m6E0D56AABAE57151C61AFDFBB696E02535AEAF22 (void);
// 0x00000023 System.Void ControlMirarCamara::Start()
extern void ControlMirarCamara_Start_mC505774AD31A2ABDBD90E6CB94FFF07C7325D838 (void);
// 0x00000024 System.Void ControlMirarCamara::Update()
extern void ControlMirarCamara_Update_mB8B66C77BFECDDFBB9790AEA60E12F1B3FA6B8CF (void);
// 0x00000025 System.Void ControlMirarCamara::.ctor()
extern void ControlMirarCamara__ctor_mD629722A3949520013D0B6113661A44EF312BFBE (void);
// 0x00000026 System.Void Curacion::Start()
extern void Curacion_Start_mD53E32139EEE73EF34AED2F37B2CB0CBC0E39E48 (void);
// 0x00000027 System.Void Curacion::OnTriggerStay(UnityEngine.Collider)
extern void Curacion_OnTriggerStay_m15C00E95FC7A8AE3809E3DCDF5711FE7A1B9AA2B (void);
// 0x00000028 System.Void Curacion::desaparecer()
extern void Curacion_desaparecer_mA1F8F13A3149BD4D53F935FDF18CAB78AEEC024E (void);
// 0x00000029 System.Void Curacion::.ctor()
extern void Curacion__ctor_m0D51954BB91B6D5F97B204A6CA1F8D83E72E2DF2 (void);
// 0x0000002A System.Void GameOverManager::Start()
extern void GameOverManager_Start_m518D33499FCC3A47F6B65751AAD8B463D3CF9360 (void);
// 0x0000002B System.Void GameOverManager::Update()
extern void GameOverManager_Update_m5B76E3EF95091F86D771D459DA67C92880BCE685 (void);
// 0x0000002C System.Void GameOverManager::CallGameOver()
extern void GameOverManager_CallGameOver_mF9E44FD060B5D935723590CB671B8901E92F7A51 (void);
// 0x0000002D System.Void GameOverManager::.ctor()
extern void GameOverManager__ctor_m4D5B6037CEF281848DC114B0A73B246F8D394ACB (void);
// 0x0000002E System.Void Meta::OnTriggerEnter(UnityEngine.Collider)
extern void Meta_OnTriggerEnter_mACCDAD9E144B2C3FEA1CDF43C7E370856E7C3625 (void);
// 0x0000002F System.Collections.IEnumerator Meta::NivelGanado()
extern void Meta_NivelGanado_mE51BEBDEDFBF234915A3BB6EC78EAD2DFB7F74FA (void);
// 0x00000030 System.Void Meta::.ctor()
extern void Meta__ctor_mBE588E417034DA37EB1C2353652FDBE99E0A82BC (void);
// 0x00000031 System.Void Meta_<NivelGanado>d__2::.ctor(System.Int32)
extern void U3CNivelGanadoU3Ed__2__ctor_m546011BE123763E6E72B1DA6333C78B9F980C4D0 (void);
// 0x00000032 System.Void Meta_<NivelGanado>d__2::System.IDisposable.Dispose()
extern void U3CNivelGanadoU3Ed__2_System_IDisposable_Dispose_m26771E1AFA0123FEFC472505974E3BA67FFABBCB (void);
// 0x00000033 System.Boolean Meta_<NivelGanado>d__2::MoveNext()
extern void U3CNivelGanadoU3Ed__2_MoveNext_mF634C7BFB864A1D4AF5DEB4FAD165D374B506E24 (void);
// 0x00000034 System.Object Meta_<NivelGanado>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNivelGanadoU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m423E1123561973ABC227CCEE56B6775CEA841FCE (void);
// 0x00000035 System.Void Meta_<NivelGanado>d__2::System.Collections.IEnumerator.Reset()
extern void U3CNivelGanadoU3Ed__2_System_Collections_IEnumerator_Reset_m5B5160ED0D53F0A6F98021F226D0A42B2EB4C08C (void);
// 0x00000036 System.Object Meta_<NivelGanado>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CNivelGanadoU3Ed__2_System_Collections_IEnumerator_get_Current_m46F8FA80C3718E60FC6A3C16D56E9B39D65B4CCE (void);
// 0x00000037 System.Void PisoInteractuables::Start()
extern void PisoInteractuables_Start_m11D89BD5B9FC7DA632D1402F1B676A764FC505A7 (void);
// 0x00000038 System.Void PisoInteractuables::OnTriggerStay(UnityEngine.Collider)
extern void PisoInteractuables_OnTriggerStay_m0E425977F99D43B129B8C387B17DA3A77135EC20 (void);
// 0x00000039 System.Void PisoInteractuables::.ctor()
extern void PisoInteractuables__ctor_m7CB603B69941EB69D96CE0EBDC93B83D1BE2CFC0 (void);
// 0x0000003A System.Void Proyectil::OntTriggerEnter(UnityEngine.Collider)
extern void Proyectil_OntTriggerEnter_m7CAFD77FEC6B5DDFA898AEF06FA5F08BF0587354 (void);
// 0x0000003B System.Void Proyectil::.ctor()
extern void Proyectil__ctor_m6D220FEC5164D502249991E3C33F5AB6855F5653 (void);
// 0x0000003C System.Void SpawnEnemigos::Start()
extern void SpawnEnemigos_Start_mE21D85CC24C0E5C34EED972E51A506BD8FAABF86 (void);
// 0x0000003D System.Void SpawnEnemigos::Update()
extern void SpawnEnemigos_Update_mFC570A2306451EC4F01B1BFBD001796B8E15AF9E (void);
// 0x0000003E System.Void SpawnEnemigos::spawnear()
extern void SpawnEnemigos_spawnear_m599AB719718390F0CE3E7937F915EC485C3EA336 (void);
// 0x0000003F System.Void SpawnEnemigos::.ctor()
extern void SpawnEnemigos__ctor_mBDE0479F99B49B909473DB20754C45FF2FCE22BC (void);
// 0x00000040 System.Void Readme::.ctor()
extern void Readme__ctor_m23AE6143BDABB863B629ADE701E2998AB8651D4C (void);
// 0x00000041 System.Void Readme_Section::.ctor()
extern void Section__ctor_mE73C1D6AE5454B5A67AAB04CAA5144A5CA0B0D96 (void);
static Il2CppMethodPointer s_methodPointers[65] = 
{
	GunView_OnEnable_mDF7A5EA7152DEE56CBE42A3865C92289F8A7F9C7,
	GunView_Update_mB2DCAD57A5AF2ED1246E7FCD25E131970FE9F395,
	GunView_ScaleUp_m4A0713AEDE03F15B8C205C75623790B8A04590B6,
	GunView_ScaleDown_m4AED203F54F6727CF3EF94FD23F669BA8DBE4378,
	GunView__ctor_m8DDE2842E8D62472387B1F2A2C84A2C3AB138AFC,
	GunsMenu_Start_m096DF81DB4260C440D835B3BC8D9C99A7F8B9B81,
	GunsMenu_NextGun_m2066B1B817B4AD3A7ECD9EA4E5EF91069588867E,
	GunsMenu_PreviousGun_m7F5F5965720D161E69B887DEC7A7C9A5BB5EA412,
	GunsMenu_Update_m1DCE6B02B9A1D113AC2EB02A6D53D3A9B84C85F5,
	GunsMenu__ctor_m1912EF0FC8BF58D7ABAE3F76FFD1D5CA856942F2,
	BarraDeVida_Update_m05AECAD272D29710D4D5ABB2406F08C8ED434F64,
	BarraDeVida__ctor_m73116A91A1BF25F308683DB3DD9B55EE78409E5C,
	Configuracion_Start_m9FD3D83B84D20DA6187983E8C21415DA55360E17,
	Configuracion_Update_m6DE8A6EF7B628D3B5F30336D16EF41496FF8747A,
	Configuracion__ctor_m516E953C50A2B4F82EC5B5C98024DA1C2BFF920B,
	ControlBot_Start_m516CAA6BE2325425DA986908B25CA69E316A0DE1,
	ControlBot_Update_mB215B939A4E82D6B449988C7EB784B9EB9BEE7E3,
	ControlBot_OnCollisionEnter_m03319A868CB93DB58D37F4DED8E2A3F2356ABE35,
	ControlBot_recibirDaUF1o_m9821E5F8B00224DE63E2BD0BF51435BDB320E7FC,
	ControlBot_desaparecer_m6D0B728AA01917F7473C2245FD960232A7E87EA3,
	ControlBot__ctor_m9C42F6EEA929FBC5B5C92A018012B8323D16D6C6,
	ControlJefe_Start_m5FA8EE6AE08EDFCC368AA2C29461C846E65CDCD4,
	ControlJefe_Update_mE41551C23C0307AEA12FD4C984F75D63B30B1513,
	ControlJefe_OnCollisionEnter_mFB4333A602D061D49942A66D693E724200EA24E9,
	ControlJefe_recibirDaUF1o_mC1510ACB15FBAF4186B3775426853456D6FF8987,
	ControlJefe_desaparecer_m40C1B5B2225D8387FEECD82633CBBF6362A002F7,
	ControlJefe__ctor_mFBC35769F5CA1F9EA4353733D535484C0C5A3A4A,
	ControlJugador_Start_m4E2BC1B8AEF94CFA13FA0A675ECC6CDD7C9E2546,
	ControlJugador_Update_m3C5ABBBBF954C1FB58E04A8E18F75DF313BE6FE8,
	ControlJugador__ctor_m8081D7912A48835FE1D245E0CA7E6DD5D2E868CB,
	ControlJugador1_Start_m4CBE46086B7ECDD4C2DF0A9AEB0C8216D94A06DD,
	ControlJugador1_update_m18022A2807EA1E23609E204132F1CE213DB1458C,
	ControlJugador1__ctor_m0AD9757AFB101D015DA3B6AB419E9BB6D58A30DB,
	ControlMenuprincipal__ctor_m6E0D56AABAE57151C61AFDFBB696E02535AEAF22,
	ControlMirarCamara_Start_mC505774AD31A2ABDBD90E6CB94FFF07C7325D838,
	ControlMirarCamara_Update_mB8B66C77BFECDDFBB9790AEA60E12F1B3FA6B8CF,
	ControlMirarCamara__ctor_mD629722A3949520013D0B6113661A44EF312BFBE,
	Curacion_Start_mD53E32139EEE73EF34AED2F37B2CB0CBC0E39E48,
	Curacion_OnTriggerStay_m15C00E95FC7A8AE3809E3DCDF5711FE7A1B9AA2B,
	Curacion_desaparecer_mA1F8F13A3149BD4D53F935FDF18CAB78AEEC024E,
	Curacion__ctor_m0D51954BB91B6D5F97B204A6CA1F8D83E72E2DF2,
	GameOverManager_Start_m518D33499FCC3A47F6B65751AAD8B463D3CF9360,
	GameOverManager_Update_m5B76E3EF95091F86D771D459DA67C92880BCE685,
	GameOverManager_CallGameOver_mF9E44FD060B5D935723590CB671B8901E92F7A51,
	GameOverManager__ctor_m4D5B6037CEF281848DC114B0A73B246F8D394ACB,
	Meta_OnTriggerEnter_mACCDAD9E144B2C3FEA1CDF43C7E370856E7C3625,
	Meta_NivelGanado_mE51BEBDEDFBF234915A3BB6EC78EAD2DFB7F74FA,
	Meta__ctor_mBE588E417034DA37EB1C2353652FDBE99E0A82BC,
	U3CNivelGanadoU3Ed__2__ctor_m546011BE123763E6E72B1DA6333C78B9F980C4D0,
	U3CNivelGanadoU3Ed__2_System_IDisposable_Dispose_m26771E1AFA0123FEFC472505974E3BA67FFABBCB,
	U3CNivelGanadoU3Ed__2_MoveNext_mF634C7BFB864A1D4AF5DEB4FAD165D374B506E24,
	U3CNivelGanadoU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m423E1123561973ABC227CCEE56B6775CEA841FCE,
	U3CNivelGanadoU3Ed__2_System_Collections_IEnumerator_Reset_m5B5160ED0D53F0A6F98021F226D0A42B2EB4C08C,
	U3CNivelGanadoU3Ed__2_System_Collections_IEnumerator_get_Current_m46F8FA80C3718E60FC6A3C16D56E9B39D65B4CCE,
	PisoInteractuables_Start_m11D89BD5B9FC7DA632D1402F1B676A764FC505A7,
	PisoInteractuables_OnTriggerStay_m0E425977F99D43B129B8C387B17DA3A77135EC20,
	PisoInteractuables__ctor_m7CB603B69941EB69D96CE0EBDC93B83D1BE2CFC0,
	Proyectil_OntTriggerEnter_m7CAFD77FEC6B5DDFA898AEF06FA5F08BF0587354,
	Proyectil__ctor_m6D220FEC5164D502249991E3C33F5AB6855F5653,
	SpawnEnemigos_Start_mE21D85CC24C0E5C34EED972E51A506BD8FAABF86,
	SpawnEnemigos_Update_mFC570A2306451EC4F01B1BFBD001796B8E15AF9E,
	SpawnEnemigos_spawnear_m599AB719718390F0CE3E7937F915EC485C3EA336,
	SpawnEnemigos__ctor_mBDE0479F99B49B909473DB20754C45FF2FCE22BC,
	Readme__ctor_m23AE6143BDABB863B629ADE701E2998AB8651D4C,
	Section__ctor_mE73C1D6AE5454B5A67AAB04CAA5144A5CA0B0D96,
};
static const int32_t s_InvokerIndices[65] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	32,
	23,
	95,
	14,
	23,
	14,
	23,
	26,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	65,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
